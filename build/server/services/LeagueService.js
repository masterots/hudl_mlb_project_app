'use strict';

var http = require('http');
var Q = require('q');
var sportradar = require('../config/sportradar');
var LeagueStore = require('../stores/LeagueStore');

var LeagueService = {
  getLeagues: function getLeagues() {
    var deferred = new Q.defer();
    if (LeagueStore.getLeagues().length === 0) {
      return this.loadLeagues();
    } else {
      deferred.resolve(LeagueStore.getLeagues());
    }
    return deferred.promise;
  },

  loadLeagues: function loadLeagues() {
    var deferred = new Q.defer();
    http.get({
      host: sportradar.host,
      path: '/mlb-t5/league/hierarchy.json?api_key=' + sportradar.apiKey
    }, function (response) {
      var body = '';
      response.on('data', function (d) {
        body += d;
      });
      response.on('end', function () {
        var leagues = undefined;
        try {
          leagues = JSON.parse(body).leagues;
          if (!leagues) {
            throw new Error('could not parse leagues');
          }
          LeagueStore.setLeagues(leagues);
          deferred.resolve(leagues);
        } catch (err) {
          console.log(err);
          deferred.reject(err);
        }
      });
    });
    return deferred.promise;
  }
};

module.exports = LeagueService;