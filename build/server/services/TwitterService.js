'use strict';

var http = require('http');
var Q = require('q');
var moment = require('moment');
var Twitter = require('twitter');
var twitterConfig = require('../config/twitter');

var client = new Twitter({
  consumer_key: twitterConfig.consumer_key,
  consumer_secret: twitterConfig.consumer_secret,
  access_token_key: twitterConfig.access_token_key,
  access_token_secret: twitterConfig.access_token_secret
});

var TwitterService = {
  getFeed: function getFeed(searchString) {
    var deferred = new Q.defer();
    client.get('search/tweets', { q: searchString }, function (error, tweets, response) {
      if (error) {
        deferred.reject(error);
      }
      deferred.resolve(tweets);
    });
    return deferred.promise;
  }
};

module.exports = TwitterService;