'use strict';

var http = require('http');
var Q = require('q');
var moment = require('moment');
var sportradar = require('../config/sportradar');

var GameService = {
  getGames: function getGames(date) {
    return this.loadGames(date);
  },

  loadGames: function loadGames(date) {
    var deferred = new Q.defer();
    http.get({
      host: sportradar.host,
      path: '/mlb-t5/games/' + moment(date).format('YYYY') + '/' + moment(date).format('MM') + '/' + moment(date).format('DD') + '/boxscore.json?api_key=' + sportradar.apiKey
    }, function (response) {
      var body = '';
      response.on('data', function (d) {
        body += d;
      });
      response.on('end', function () {
        var games = undefined;
        try {
          games = JSON.parse(body).league.games;
          if (!games) {
            throw new Error('could not parse games');
          }
          deferred.resolve(games);
        } catch (err) {
          console.log(err);
          deferred.reject(err);
        }
      });
    }).on('error', function (err) {
      console.log(err);
      deferred.reject(err);
    });
    return deferred.promise;
  }
};

module.exports = GameService;