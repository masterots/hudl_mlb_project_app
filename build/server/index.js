'use strict';

var express = require('express');
var path = require('path');
var app = express();
var compress = require('compression');
var methodOverride = require('method-override');
// let bodyParser = require('body-parser');

var site = require('./routes/site');
var games = require('./routes/games');
var twitter = require('./routes/twitter');

app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(methodOverride('_method'));

app.use(compress());
// app.use(bodyParser.json()); // for parsing application/json
// app.use(bodyParser.urlencoded({ extended: true }));
app.use(express['static'](path.join(__dirname, '..', 'public')));

app.use('/', site);
app.use('/games', games);
app.use('/twitter', twitter);

app.use(function (req, res, next) {
  res.status(404).send('Sorry cant find that!');
});

app.set('port', process.env.PORT || 3000);
var server = app.listen(app.get('port'), function () {
  console.log('Express server listening on port ' + server.address().port);
});