"use strict";

var _leagues = [];

var LeagueStore = {
  setLeagues: function setLeagues(leagues) {
    _leagues = leagues;
  },

  getLeagues: function getLeagues() {
    return _leagues;
  }
};

module.exports = LeagueStore;