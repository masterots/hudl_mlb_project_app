'use strict';

var express = require('express');
var router = express.Router();
var GameService = require('../services/GameService');

router.get('/', function (req, res) {
  if (!req.query.date) {
    res.status(400).send('Please include ?date=YYYY-MM-DD');
  }
  GameService.getGames(req.query.date).then(function (games) {
    return res.json(games);
  })['catch'](function (err) {
    // logging!
    res.status(500).send('There was an error retrieving games.');
  }).done();
});

module.exports = router;