'use strict';

var express = require('express');
var router = express.Router();
var LeagueService = require('../services/LeagueService');

router.get('/', function (req, res) {
  LeagueService.getLeagues().then(function (leagues) {
    return res.json(leagues);
  })['catch'](function (err) {
    // logging!
    res.status(500).send('There was an error retrieving leagues.');
  }).done();
});

module.exports = router;