'use strict';

var express = require('express');
var router = express.Router();
var TwitterService = require('../services/TwitterService');

router.get('/', function (req, res) {
  var twitterSearchString = req.query.search ? req.query.search : '';
  TwitterService.getFeed(twitterSearchString).then(function (feed) {
    return res.json(feed);
  })['catch'](function (err) {
    // logging!
    res.status(500).send('There was an error retrieving the twitter feed.');
  }).done();
});

module.exports = router;