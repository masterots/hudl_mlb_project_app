'use strict';

(function () {
  'use strict';
  angular.module('hudl_mlb', []);
})();
'use strict';

(function () {
  'use strict';

  angular.module('hudl_mlb').controller('HudlMlbController', HudlMlbController);

  HudlMlbController.$inject = ['$scope', '$interval', 'GameService', 'DateService'];
  function HudlMlbController($scope, $interval, GameService, DateService) {
    var vm = this;
    var tenMinuteTimer = 1000 * 60 * 10;

    vm.games = GameService.getGames();
    vm.gamedate = moment().format('YYYY-MM-DD');
    vm.loadGames = loadGames;
    vm.selectedGame = GameService.getSelectedGame();

    loadGames();

    function loadGames() {
      DateService.setDate(vm.gamedate);
      GameService.setSelectedGame({});
      GameService.loadGames(vm.gamedate);
    }

    $scope.$on('games:updated', function () {
      return vm.games = GameService.getGames();
    });
    $scope.$on('games:selectedChanged', function () {
      return vm.selectedGame = GameService.getSelectedGame();
    });

    $interval(function () {
      GameService.loadGames(DateService.getDate());
    }, tenMinuteTimer);
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('hudl_mlb').directive('gamePanelList', gamePanelListDirective);

  function gamePanelListDirective(GameService) {
    var directive = {
      replace: true,
      scope: {
        games: '='
      },
      template: '<div class="game-list-mini-panels">\n                    <game-panel-mini\n                        game="game"\n                        ng-repeat="game in games | orderBy: \'scheduled\'"\n                      ></game-panel-mini>\n                  </div>'
    };

    return directive;
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('hudl_mlb').directive('gamePanelMini', gamePanelMiniDirective);

  function gamePanelMiniDirective() {
    var directive = {
      controller: GamePanelMiniController,
      controllerAs: 'gpm',
      replace: true,
      scope: {
        game: '='
      },
      template: '<div class="game-panel-mini" ng-class="{\'selected\': gpm.isSelected()}" ng-click="gpm.handleClick()">\n                    <div class="home-team">\n                      <img class="team-logo" ng-src="{{gpm.game.home.logo}}" />\n                      <h3 class="team-name">{{gpm.game.home.abbr}}</h3>\n                      <div class="score" ng-if="gpm.game.status!==\'scheduled\'">{{gpm.game.home.runs}}</div>\n                    </div>\n                    <div class="away-team">\n                      <img class="team-logo" ng-src="{{gpm.game.away.logo}}" />\n                      <h3 class="team-name">{{gpm.game.away.abbr}}</h3>\n                      <div class="score" ng-if="gpm.game.status!==\'scheduled\'">{{gpm.game.away.runs}}</div>\n                    </div>\n                    <game-panel-mini-final ng-if="gpm.game.status===\'closed\'"></game-panel-mini-final>\n                    <game-panel-mini-scheduled ng-if="gpm.game.status===\'scheduled\'"></game-panel-mini-scheduled>\n                    <game-panel-mini-inning ng-if="gpm.game.status===\'inprogress\'"></game-panel-mini-inning>\n                  </div>'
    };

    return directive;
  }

  GamePanelMiniController.$inject = ['$scope', 'GameService'];
  function GamePanelMiniController($scope, GameService) {
    var vm = this;
    vm.game = $scope.game;
    vm.handleClick = handleClick;
    vm.isSelected = isSelected;
    vm.selectedGame = GameService.getSelectedGame();
    $scope.$on('games:selectedChanged', function () {
      return vm.selectedGame = GameService.getSelectedGame();
    });

    function handleClick() {
      GameService.setSelectedGame(vm.game);
    }

    function isSelected() {
      return vm.game.id === vm.selectedGame.id;
    }
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('hudl_mlb').directive('gamePanelMiniFinal', gamePanelMiniFinal);

  function gamePanelMiniFinal() {
    var directive = {
      replace: true,
      template: '<div class="status final">\n                  Final\n                 </div>'
    };
    return directive;
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('hudl_mlb').directive('gamePanelMiniInning', gamePanelMiniInning);

  function gamePanelMiniInning() {
    var directive = {
      replace: true,
      template: '<div class="status inning">\n                  <span ng-if="game.outcome.current_inning_half === \'T\'">Top</span>\n                  <span ng-if="game.outcome.current_inning_half === \'B\'">Bottom</span>\n                  <span>{{game.outcome.current_inning}}</span>\n                 </div>'
    };
    return directive;
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('hudl_mlb').directive('gamePanelMiniScheduled', gamePanelMiniScheduled);

  function gamePanelMiniScheduled() {
    var directive = {
      link: link,
      replace: true,
      template: '<div class="status final">\n                  {{gameTime}}\n                 </div>'
    };
    return directive;

    function link(scope) {
      scope.gameTime = moment(scope.game.scheduled).format('hh:mm a');
    }
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('hudl_mlb').directive('scoreboard', scoreboardDirective);

  function scoreboardDirective() {
    var directive = {
      controller: ScoreboardController,
      controllerAs: 'scoreboard',
      template: '<div class="scoreboard">\n                  <table class="pure-table pure-table-bordered">\n                    <thead>\n                      <tr>\n                        <th>Team</th>\n                        <th ng-repeat="inning in scoreboard.innings track by $index">{{inning}}</th>\n                        <th>R</th>\n                        <th>H</th>\n                        <th>E</th>\n                      </tr>\n                    </thead>\n                    <tbody>\n                      <tr>\n                        <td>{{scoreboard.game.away.name}}</td>\n                        <td ng-repeat="score in scoreboard.awayTeamScores track by $index">{{score}}</td>\n                        <td>{{scoreboard.game.away.runs}}</td>\n                        <td>{{scoreboard.game.away.hits}}</td>\n                        <td>{{scoreboard.game.away.errors}}</td>\n                      </tr>\n                      <tr>\n                        <td>{{scoreboard.game.home.name}}</td>\n                        <td ng-repeat="score in scoreboard.homeTeamScores track by $index">{{score}}</td>\n                        <td>{{scoreboard.game.home.runs}}</td>\n                        <td>{{scoreboard.game.home.hits}}</td>\n                        <td>{{scoreboard.game.home.errors}}</td>\n                      </tr>\n                    </tbody>\n                  </table>\n                 </div>'
    };
    return directive;
  }

  ScoreboardController.$inject = ['$scope'];
  function ScoreboardController($scope) {
    var vm = this;
    vm.game = {};
    vm.innings = [];
    vm.homeTeamScores = [];
    vm.awayTeamScores = [];

    updateBoard($scope.selectedGame);

    function updateBoard(selectedGame) {
      vm.innings = [];
      vm.homeTeamScores = [];
      vm.awayTeamScores = [];
      vm.game = selectedGame;
      if (vm.game.status === 'closed') {
        vm.innings = vm.game.home.scoring.map(function (inning, index) {
          return index + 1;
        });;
        vm.homeTeamScores = vm.game.home.scoring.map(function (inning) {
          return noXChar(inning.runs);
        });
        vm.awayTeamScores = vm.game.away.scoring.map(function (inning) {
          return noXChar(inning.runs);
        });
      } else {
        var numInnings = vm.game.outcome.current_inning < 9 ? 9 : vm.game.outcome.current_inning;
        for (var i = 1; i <= numInnings; i++) {
          vm.innings.push(i);
          var homeTeamScore = vm.game.home.scoring[i - 1] ? noXChar(vm.game.home.scoring[i - 1].runs) : '';
          var awayTeamScore = vm.game.away.scoring[i - 1] ? noXChar(vm.game.away.scoring[i - 1].runs) : '';
          vm.homeTeamScores.push(homeTeamScore);
          vm.awayTeamScores.push(awayTeamScore);
        }
      }
    }

    function noXChar(runs) {
      if (runs === 'X') {
        return runs.replace('X', '');
      }
      return runs;
    }

    $scope.$watch('selectedGame', function (selectedGame) {
      return updateBoard(selectedGame);
    });
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('hudl_mlb').directive('selectedGamePanel', selectedGamePanelDirective);

  function selectedGamePanelDirective() {
    var directive = {
      controller: SelectedGamePanelController,
      controllerAs: 'sgp',
      replace: true,
      scope: {
        selectedGame: '='
      },
      template: '<div class="selected-game-panel">\n                  <div class="teams">\n                    <div class="away-team">\n                      <img class="team-logo" ng-src="{{sgp.game.away.logo}}" />\n                      <h3 class="team-name">{{sgp.game.away.market}} {{sgp.game.away.name}}</h3>\n                      <div class="score" ng-if="sgp.game.status!==\'scheduled\'">{{sgp.game.away.runs}}</div>\n                    </div>\n                    <div class="home-team">\n                      <img class="team-logo" ng-src="{{sgp.game.home.logo}}" />\n                      <h3 class="team-name">{{sgp.game.home.market}} {{sgp.game.home.name}}</h3>\n                      <div class="score" ng-if="sgp.game.status!==\'scheduled\'">{{sgp.game.home.runs}}</div>\n                    </div>\n                  </div>\n                  <scoreboard ng-if="sgp.game.status!==\'scheduled\'"></scoreboard>\n                </div>'
    };
    return directive;
  }

  SelectedGamePanelController.$inject = ['$scope'];
  function SelectedGamePanelController($scope) {
    var vm = this;
    vm.game = $scope.selectedGame;

    $scope.$watch('selectedGame', function (selectedGame) {
      return vm.game = selectedGame;
    });
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('hudl_mlb').directive('selectedGameTwitterFeed', selectedGameTwitterFeedDirective);

  function selectedGameTwitterFeedDirective() {
    var directive = {
      controller: SelectedGameTwitterFeedController,
      controllerAs: 'sgtf',
      replace: true,
      scope: {
        selectedGame: '='
      },
      template: '<div class="twitter-posts">\n                  <h3>Twitter</h3>\n                  <div class="post"  ng-repeat="post in sgtf.twitterFeed track by $index">\n                    <img ng-src={{post.user.profile_image_url}} alt="{{post.user.name}}"/>\n                    <div class="user"><a href="http://twitter.com/{{post.user.screen_name}}" target="_blank">{{post.user.name}}</a></div>\n                    <div class="text">{{post.text}}</div>\n                  </div>\n                </div>'
    };
    return directive;
  }

  SelectedGameTwitterFeedController.$inject = ['$scope', '$interval', 'TwitterService'];
  function SelectedGameTwitterFeedController($scope, $interval, TwitterService) {
    var vm = this;
    vm.game = $scope.selectedGame;
    vm.twitterFeed = [];

    $interval(function () {
      getTwitterFeed();
    }, 10000);

    $scope.$watch('selectedGame', function (selectedGame) {
      vm.game = selectedGame;
      getTwitterFeed();
    });

    function concatTeamNames() {
      return encodeURI('' + vm.game.home.market + ' ' + vm.game.home.name + ' ' + vm.game.away.market + ' ' + vm.game.away.name);
    }

    function getTwitterFeed() {
      TwitterService.getFeed(concatTeamNames()).then(function (feed) {
        vm.twitterFeed = feed.data.statuses;
      });
    }
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('hudl_mlb').service('DateService', DateService);

  function DateService() {
    var _date = moment().format('YYYY-MM-DD');
    var service = {
      getDate: getDate,
      setDate: setDate
    };

    return service;

    function getDate() {
      return _date;
    }

    function setDate(date) {
      if (moment(date).isValid()) {
        _date = date;
      }
    }
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('hudl_mlb').service('GameService', GameService);

  GameService.$inject = ['$rootScope', '$http'];
  function GameService($rootScope, $http) {
    var _games = [];
    var _selectedGame = {};

    var service = {
      getGames: getGames,
      setGames: setGames,
      getSelectedGame: getSelectedGame,
      setSelectedGame: setSelectedGame,
      loadGames: loadGames
    };
    return service;

    function setGames(games) {
      _games = _.map(games, function (game) {
        return _setTeamLogoUrl(game.game);
      });
    }

    function setSelectedGame(game) {
      _selectedGame = game;
      _sendSelectedChanged();
    }

    function _setTeamLogoUrl(game) {
      game.home.logo = '/images/' + game.home.name.toLowerCase().replace(/\s/g, '_') + '.gif';
      game.away.logo = '/images/' + game.away.name.toLowerCase().replace(/\s/g, '_') + '.gif';
      return game;
    }

    function _sendGamesUpdated() {
      $rootScope.$broadcast('games:updated');
    }

    function _sendSelectedChanged() {
      $rootScope.$broadcast('games:selectedChanged');
    }

    function getGames() {
      return _games;
    }

    function getSelectedGame() {
      return _selectedGame;
    }

    function loadGames(date) {
      $http.get('/games?date=' + date).success(function (games) {
        setGames(games);
        _sendGamesUpdated();
      }).error(function (err) {
        return console.log(err);
      });
    }
  }
})();
'use strict';

(function () {
  'use strict';

  angular.module('hudl_mlb').service('TwitterService', TwitterService);

  TwitterService.$inject = ['$http'];
  function TwitterService($http) {
    var service = {
      getFeed: getFeed
    };
    return service;

    function getFeed(searchString) {
      return $http.get('/twitter?search=' + searchString).success(function (feed) {
        return feed;
      }).error(function (err) {
        return err;
      });
    }
  }
})();