(function() {
  'use strict';

  angular
    .module('hudl_mlb')
    .directive('scoreboard', scoreboardDirective);

  function scoreboardDirective() {
    let directive = {
      controller: ScoreboardController,
      controllerAs: 'scoreboard',
      template: `<div class="scoreboard">
                  <table class="pure-table pure-table-bordered">
                    <thead>
                      <tr>
                        <th>Team</th>
                        <th ng-repeat="inning in scoreboard.innings track by $index">{{inning}}</th>
                        <th>R</th>
                        <th>H</th>
                        <th>E</th>
                      </tr>
                    </thead>
                    <tbody>
                      <tr>
                        <td>{{scoreboard.game.away.name}}</td>
                        <td ng-repeat="score in scoreboard.awayTeamScores track by $index">{{score}}</td>
                        <td>{{scoreboard.game.away.runs}}</td>
                        <td>{{scoreboard.game.away.hits}}</td>
                        <td>{{scoreboard.game.away.errors}}</td>
                      </tr>
                      <tr>
                        <td>{{scoreboard.game.home.name}}</td>
                        <td ng-repeat="score in scoreboard.homeTeamScores track by $index">{{score}}</td>
                        <td>{{scoreboard.game.home.runs}}</td>
                        <td>{{scoreboard.game.home.hits}}</td>
                        <td>{{scoreboard.game.home.errors}}</td>
                      </tr>
                    </tbody>
                  </table>
                 </div>`
    };
    return directive;
  }

  ScoreboardController.$inject = ['$scope'];
  function ScoreboardController($scope) {
    let vm = this;
    vm.game = {};
    vm.innings = [];
    vm.homeTeamScores = [];
    vm.awayTeamScores = [];

    updateBoard($scope.selectedGame)

    function updateBoard(selectedGame) {
      vm.innings = [];
      vm.homeTeamScores = [];
      vm.awayTeamScores = [];
      vm.game = selectedGame;
      if (vm.game.status === 'closed') {
        vm.innings = vm.game.home.scoring.map((inning, index) => index + 1);;
        vm.homeTeamScores = vm.game.home.scoring.map(inning => noXChar(inning.runs));
        vm.awayTeamScores = vm.game.away.scoring.map(inning => noXChar(inning.runs));
      } else {
        let numInnings = vm.game.outcome.current_inning < 9 ? 9 : vm.game.outcome.current_inning;
        for (let i = 1; i <= numInnings; i++) {
          vm.innings.push(i);
          let homeTeamScore = vm.game.home.scoring[i-1] ? noXChar(vm.game.home.scoring[i-1].runs) : '';
          let awayTeamScore = vm.game.away.scoring[i-1] ? noXChar(vm.game.away.scoring[i-1].runs) : '';
          vm.homeTeamScores.push(homeTeamScore);
          vm.awayTeamScores.push(awayTeamScore);
        }
      }
    }

    function noXChar(runs) {
      if (runs === 'X') {
        return runs.replace('X', '');
      }
      return runs;
    }

    $scope.$watch('selectedGame', selectedGame => updateBoard(selectedGame));
  }
}());