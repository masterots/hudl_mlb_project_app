(function() {
  'use strict';

  angular
    .module('hudl_mlb')
    .directive('selectedGameTwitterFeed', selectedGameTwitterFeedDirective);

  function selectedGameTwitterFeedDirective() {
    let directive = {
      controller: SelectedGameTwitterFeedController,
      controllerAs: 'sgtf',
      replace: true,
      scope: {
        selectedGame: '='
      },
      template: `<div class="twitter-posts">
                  <h3>Twitter</h3>
                  <div class="post"  ng-repeat="post in sgtf.twitterFeed track by $index">
                    <img ng-src={{post.user.profile_image_url}} alt="{{post.user.name}}"/>
                    <div class="user"><a href="http://twitter.com/{{post.user.screen_name}}" target="_blank">{{post.user.name}}</a></div>
                    <div class="text">{{post.text}}</div>
                  </div>
                </div>`
    };
    return directive;
  }

  SelectedGameTwitterFeedController.$inject = ['$scope', '$interval', 'TwitterService'];
  function SelectedGameTwitterFeedController($scope, $interval, TwitterService) {
    let vm = this;
    vm.game = $scope.selectedGame;
    vm.twitterFeed = [];

    $interval(() => {
      getTwitterFeed();
    }, 10000);

    $scope.$watch('selectedGame', selectedGame => {
      vm.game = selectedGame;
      getTwitterFeed();
    });

    function concatTeamNames() {
      return encodeURI(`${vm.game.home.market} ${vm.game.home.name} ${vm.game.away.market} ${vm.game.away.name}`);
    }

    function getTwitterFeed() {
      TwitterService.getFeed(concatTeamNames())
        .then(feed => {
          vm.twitterFeed = feed.data.statuses;
        });
    }
  }
}());
