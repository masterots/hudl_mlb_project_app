(function() {
  'use strict';

  angular
    .module('hudl_mlb')
    .directive('gamePanelMiniFinal', gamePanelMiniFinal);

  function gamePanelMiniFinal() {
    let directive = {
      replace: true,
      template: `<div class="status final">
                  Final
                 </div>`
    };
    return directive;
  }
}());
