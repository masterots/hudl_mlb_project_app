(function() {
  'use strict';

  angular
    .module('hudl_mlb')
    .directive('gamePanelMini', gamePanelMiniDirective);

    function gamePanelMiniDirective() {
      let directive = {
        controller: GamePanelMiniController,
        controllerAs: 'gpm',
        replace: true,
        scope: {
          game: '='
        },
        template: `<div class="game-panel-mini" ng-class="{'selected': gpm.isSelected()}" ng-click="gpm.handleClick()">
                    <div class="home-team">
                      <img class="team-logo" ng-src="{{gpm.game.home.logo}}" />
                      <h3 class="team-name">{{gpm.game.home.abbr}}</h3>
                      <div class="score" ng-if="gpm.game.status!=='scheduled'">{{gpm.game.home.runs}}</div>
                    </div>
                    <div class="away-team">
                      <img class="team-logo" ng-src="{{gpm.game.away.logo}}" />
                      <h3 class="team-name">{{gpm.game.away.abbr}}</h3>
                      <div class="score" ng-if="gpm.game.status!=='scheduled'">{{gpm.game.away.runs}}</div>
                    </div>
                    <game-panel-mini-final ng-if="gpm.game.status==='closed'"></game-panel-mini-final>
                    <game-panel-mini-scheduled ng-if="gpm.game.status==='scheduled'"></game-panel-mini-scheduled>
                    <game-panel-mini-inning ng-if="gpm.game.status==='inprogress'"></game-panel-mini-inning>
                  </div>`
      };

      return directive;
    }

    GamePanelMiniController.$inject = ['$scope', 'GameService'];
    function GamePanelMiniController($scope, GameService) {
      let vm = this;
      vm.game = $scope.game;
      vm.handleClick = handleClick;
      vm.isSelected = isSelected;
      vm.selectedGame = GameService.getSelectedGame();
      $scope.$on('games:selectedChanged', () => vm.selectedGame = GameService.getSelectedGame());

      function handleClick() {
        GameService.setSelectedGame(vm.game);
      }

      function isSelected() {
        return vm.game.id === vm.selectedGame.id;
      }
    }

}());
