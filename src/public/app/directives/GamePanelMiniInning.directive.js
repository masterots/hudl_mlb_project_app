(function() {
  'use strict';

  angular
    .module('hudl_mlb')
    .directive('gamePanelMiniInning', gamePanelMiniInning);

  function gamePanelMiniInning() {
    let directive = {
      replace: true,
      template: `<div class="status inning">
                  <span ng-if="game.outcome.current_inning_half === 'T'">Top</span>
                  <span ng-if="game.outcome.current_inning_half === 'B'">Bottom</span>
                  <span>{{game.outcome.current_inning}}</span>
                 </div>`
    };
    return directive;
  }
}());
