(function() {
  'use strict';

  angular
    .module('hudl_mlb')
    .directive('gamePanelList', gamePanelListDirective);

    function gamePanelListDirective(GameService) {
      let directive = {
        replace: true,
        scope: {
          games: '='
        },
        template: `<div class="game-list-mini-panels">
                    <game-panel-mini
                        game="game"
                        ng-repeat="game in games | orderBy: 'scheduled'"
                      ></game-panel-mini>
                  </div>`
      };

      return directive;
    }

}());
