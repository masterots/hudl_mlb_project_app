(function() {
  'use strict';

  angular
    .module('hudl_mlb')
    .directive('selectedGamePanel', selectedGamePanelDirective);

  function selectedGamePanelDirective() {
    let directive = {
      controller: SelectedGamePanelController,
      controllerAs: 'sgp',
      replace: true,
      scope: {
        selectedGame: '='
      },
      template: `<div class="selected-game-panel">
                  <div class="teams">
                    <div class="away-team">
                      <img class="team-logo" ng-src="{{sgp.game.away.logo}}" />
                      <h3 class="team-name">{{sgp.game.away.market}} {{sgp.game.away.name}}</h3>
                      <div class="score" ng-if="sgp.game.status!=='scheduled'">{{sgp.game.away.runs}}</div>
                    </div>
                    <div class="home-team">
                      <img class="team-logo" ng-src="{{sgp.game.home.logo}}" />
                      <h3 class="team-name">{{sgp.game.home.market}} {{sgp.game.home.name}}</h3>
                      <div class="score" ng-if="sgp.game.status!=='scheduled'">{{sgp.game.home.runs}}</div>
                    </div>
                  </div>
                  <scoreboard ng-if="sgp.game.status!=='scheduled'"></scoreboard>
                </div>`
    };
    return directive;
  }

  SelectedGamePanelController.$inject = ['$scope'];
  function SelectedGamePanelController($scope) {
    let vm = this;
    vm.game = $scope.selectedGame;

    $scope.$watch('selectedGame', selectedGame => vm.game = selectedGame);
  }
}());
