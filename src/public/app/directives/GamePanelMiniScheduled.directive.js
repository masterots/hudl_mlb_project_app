(function() {
  'use strict';

  angular
    .module('hudl_mlb')
    .directive('gamePanelMiniScheduled', gamePanelMiniScheduled);

  function gamePanelMiniScheduled() {
    let directive = {
      link: link,
      replace: true,
      template: `<div class="status final">
                  {{gameTime}}
                 </div>`
    };
    return directive;

    function link(scope) {
      scope.gameTime = moment(scope.game.scheduled).format('hh:mm a');
    }
  }
}());
