(function() {
  'use strict';

  angular
    .module('hudl_mlb')
    .controller('HudlMlbController', HudlMlbController);

  HudlMlbController.$inject = ['$scope', '$interval', 'GameService', 'DateService'];
  function HudlMlbController($scope, $interval, GameService, DateService) {
    let vm = this;
    let tenMinuteTimer = 1000 * 60 * 10;

    vm.games = GameService.getGames();
    vm.gamedate = moment().format('YYYY-MM-DD');
    vm.loadGames = loadGames;
    vm.selectedGame = GameService.getSelectedGame();

    loadGames();

    function loadGames() {
      DateService.setDate(vm.gamedate);
      GameService.setSelectedGame({});
      GameService.loadGames(vm.gamedate);
    }

    $scope.$on('games:updated', () => vm.games = GameService.getGames());
    $scope.$on('games:selectedChanged', () => vm.selectedGame = GameService.getSelectedGame());

    $interval(() => {
      GameService.loadGames(DateService.getDate());
    }, tenMinuteTimer);
  }
}());
