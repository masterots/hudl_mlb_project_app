(function() {
  'use strict';

  angular
    .module('hudl_mlb')
    .service('TwitterService', TwitterService);

  TwitterService.$inject = ['$http'];
  function TwitterService($http) {
    let service = {
      getFeed: getFeed
    };
    return service;


    function getFeed(searchString) {
      return $http.get(`/twitter?search=${searchString}`)
        .success(feed => feed)
        .error(err => err);
    }
  }
}());
