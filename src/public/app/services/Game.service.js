(function() {
  'use strict';

  angular
    .module('hudl_mlb')
    .service('GameService', GameService);

  GameService.$inject = ['$rootScope', '$http'];
  function GameService($rootScope, $http) {
    let _games = [];
    let _selectedGame = {};

    let service = {
      getGames: getGames,
      setGames: setGames,
      getSelectedGame: getSelectedGame,
      setSelectedGame: setSelectedGame,
      loadGames: loadGames
    };
    return service;


    function setGames(games) {
      _games = _.map(games, game => _setTeamLogoUrl(game.game));
    }

    function setSelectedGame(game) {
      _selectedGame = game;
      _sendSelectedChanged();
    }

    function _setTeamLogoUrl(game) {
      game.home.logo = `/images/${game.home.name.toLowerCase().replace(/\s/g, '_')}.gif`;
      game.away.logo = `/images/${game.away.name.toLowerCase().replace(/\s/g, '_')}.gif`;
      return game;
    }

    function _sendGamesUpdated() {
      $rootScope.$broadcast('games:updated');
    }

    function _sendSelectedChanged() {
      $rootScope.$broadcast('games:selectedChanged');
    }

    function getGames() {
      return _games;
    }

    function getSelectedGame() {
      return _selectedGame;
    }

    function loadGames(date) {
      $http.get(`/games?date=${date}`)
        .success(games => {
          setGames(games);
          _sendGamesUpdated();
        })
        .error(err => console.log(err));
    }
  }
}());
