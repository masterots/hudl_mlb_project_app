(function () {
  'use strict';

  angular
    .module('hudl_mlb')
    .service('DateService', DateService);

  function DateService() {
    let _date = moment().format('YYYY-MM-DD');
    let service = {
      getDate: getDate,
      setDate: setDate
    };

    return service;

    function getDate() {
      return _date;
    }

    function setDate(date) {
      if (moment(date).isValid()) {
        _date = date;
      }
    }
  }
})();