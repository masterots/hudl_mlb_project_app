let http = require('http');
let Q = require('q');
let moment = require('moment');
let sportradar = require('../config/sportradar');

let GameService = {
  getGames(date) {
    return this.loadGames(date);
  },

  loadGames(date) {
    let deferred = new Q.defer();
    http.get({
        host: sportradar.host,
        path: `/mlb-t5/games/${moment(date).format('YYYY')}/${moment(date).format('MM')}/${moment(date).format('DD')}/boxscore.json?api_key=${sportradar.apiKey}`
    }, response => {
        let body = '';
        response.on('data', function(d) {
            body += d;
        });
        response.on('end', function() {
          let games;
          try {
            games = JSON.parse(body).league.games;
            if (!games) {
              throw new Error('could not parse games');
            }
            deferred.resolve(games);
          } catch (err) {
            console.log(err);
            deferred.reject(err);
          }
        });
    }).on('error', err => {
      console.log(err);
      deferred.reject(err);
    });
    return deferred.promise;
  }
};

module.exports = GameService;
