let http = require('http');
let Q = require('q');
let moment = require('moment');
let Twitter = require('twitter');
let twitterConfig = require('../config/twitter');

let client = new Twitter({
  consumer_key: twitterConfig.consumer_key,
  consumer_secret: twitterConfig.consumer_secret,
  access_token_key: twitterConfig.access_token_key,
  access_token_secret: twitterConfig.access_token_secret
});

let TwitterService = {
  getFeed(searchString) {
    let deferred = new Q.defer();
    client.get('search/tweets', {q: searchString}, function(error, tweets, response){
      if (error) {
        deferred.reject(error);
      }
      deferred.resolve(tweets);
    });
    return deferred.promise;
  }
};

module.exports = TwitterService;
