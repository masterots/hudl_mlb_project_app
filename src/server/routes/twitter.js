let express = require('express');
let router = express.Router();
let TwitterService = require('../services/TwitterService');

router.get('/', (req, res) => {
  let twitterSearchString = req.query.search ? req.query.search : '';
  TwitterService.getFeed(twitterSearchString)
  .then(feed => res.json(feed))
  .catch(err => {
    // logging!
    res.status(500).send('There was an error retrieving the twitter feed.');
  })
  .done();
});

module.exports = router;
