let express = require('express');
let router = express.Router();
let GameService = require('../services/GameService');

router.get('/', (req, res) => {
  if (!req.query.date) {
    res.status(400).send('Please include ?date=YYYY-MM-DD');
  }
  GameService.getGames(req.query.date)
  .then(games => res.json(games))
  .catch(err => {
    // logging!
    res.status(500).send('There was an error retrieving games.');
  })
  .done();
});

module.exports = router;
