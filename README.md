# The Hudl MLB Project

[hudlmlb.herokuapp.com](http://hudlmlb.herokuapp.com)

[Source code](https://bitbucket.org/masterots/hudl_mlb_project_app)

For my project I chose to integrate a day's MLB box scores alonside a Twitter search feed whose query contains the names of the two teams involved in the selected game. For box scores, I found a service from [sportradar.us] (http://developer.sportradar.us/io-docs). 

The times for the games shown are not adjusted for the end user timezone right now, they are shown for the timezone set on the hosting server.

I included a couple of simple unit tests for an Angular service and an Angular Directive, to show that I understand and can write tests, but I didn't want to write a comprehensive test suite for the application yet.

I would also normally add in some logging, using modules such as Winston or Bunyan when building an application that I'd put into production, and not just print to the console.

## Parts of the app 


- Scrollable list of games with score, start time if not started, inning if started, and final if game over
- On game selected, shows box score for game along with innings
- On game selected, shows scrollable list of tweets returned by search composed of team names
- Games are refreshed every 10 minutes, since the Sportradar API trial is limited to 1000 hits per month
- Twitter search results are updated every 10 seconds

## Frameworks

- Express/Node.js
- AngularJS
- PureCSS
- lodash
- Q (promises)

## Tooling

- Gulp
- Karma
- Jasmine
- PhantomJS
- NPM
- Bower
- Babel

## To run the app
- Node.js
- globally installed
 - bower
 - gulp
 - phantomjs
 - karma-cli

Once those requirements have been met, you should be able to run the following commands:

    bower install
    npm install
    gulp
    
Application should then be running at [http://localhost:3000](http://localhost:3000)


