var gulp = require('gulp');

require('./gulp/angular-app');
require('./gulp/node-app');
require('./gulp/watch');
require('./gulp/styles');
require('./gulp/node-serve');
require('./gulp/copy-files');

gulp.task('develop', [
  'angular:app:develop',
  'sass:develop',
  'node:develop',
  'copy:public:files',
  'copy:server:files'
]);

gulp.task('deploy', [
  'angular:app:deploy',
  'sass:deploy',
  'copy:public:files',
  'copy:server:files'
]);

gulp.task('default', ['develop'], function() {
  return gulp.start('node:serve', 'watch');
});
