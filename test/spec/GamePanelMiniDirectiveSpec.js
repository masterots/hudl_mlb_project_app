(function() {
  'use strict';

  describe('GamePanelMiniDirective', function() {
    var element, scope;

    beforeEach(module('hudl_mlb'));

    beforeEach(inject(function($rootScope, $compile) {
      var game = {
        id: "719a653a-f946-423e-b307-f74e9b3e82a4",
        status: "inprogress",
        coverage: "full",
        game_number: 1,
        day_night: "D",
        scheduled: "2015-05-03T18:10:00+00:00",
        home_team: "833a51a9-0d84-410f-bd77-da08c3e5e26e",
        away_team: "575c19b7-4052-41c2-9f0a-1c5813d02f99",
        venue: { id: "6fca95c9-7f2c-4acb-a9f3-02ef96340d2a", name: "Kauffman Stadium",
                market: "Kansas City", capacity: 37903, surface: "grass",
                address: "One Royal Way", city: "Kansas City", state: "MO",
                zip: "64129", country: "USA"
        },
        outcome: {
          type: "pitch",
          current_inning: 7,
          current_inning_half: "B",
          count: { strikes: 2,  balls: 1,  outs: 1,  inning: 7,  inning_half: "B",  half_over: false }
        },
        home: {
          name: "Royals",
          market: "Kansas City",
          abbr: "KC",
          id: "833a51a9-0d84-410f-bd77-da08c3e5e26e",
          runs: 0,
          hits: 1,
          errors: 1,
          scoring: [
            { number: 1,  sequence: 1,  runs: 0,  type: "inning" },
            { number: 2,  sequence: 2,  runs: 0,  type: "inning" },
            { number: 3,  sequence: 3,  runs: 0,  type: "inning" },
            { number: 4,  sequence: 4,  runs: 0,  type: "inning" },
            { number: 5,  sequence: 5,  runs: 0,  type: "inning" },
            { number: 6,  sequence: 6,  runs: 0,  type: "inning" },
            { number: 7,  sequence: 7,  runs: 0,  type: "inning" }
          ]
        },
        away: {
          name: "Tigers",
          market: "Detroit",
          abbr: "DET",
          id: "575c19b7-4052-41c2-9f0a-1c5813d02f99",
          runs: 6,
          hits: 11,
          errors: 0,
          scoring: [
            { number: 1,  sequence: 1,  runs: 0,  type: "inning" },
            { number: 2,  sequence: 2,  runs: 1,  type: "inning" },
            { number: 3,  sequence: 3,  runs: 0,  type: "inning" },
            { number: 4,  sequence: 4,  runs: 3,  type: "inning" },
            { number: 5,  sequence: 5,  runs: 2,  type: "inning" },
            { number: 6,  sequence: 6,  runs: 0,  type: "inning" },
            { number: 7,  sequence: 7,  runs: 0,  type: "inning" }
          ]
        }
      };
      scope = $rootScope.$new();
      scope.game = game;

      element = '<game-panel-mini game="game"></game-panel-mini>';
      element = $compile(element)(scope);

      scope.$digest();
    }));

    it('should do something', function() {
      var elementText = $(element).find('.status').text().replace(/\s/g, '');
      expect(elementText).toBe('Bottom7');
    });

  });
}());