(function() {
  'use strict';

  describe('GameService', function() {
    var GameService;

    beforeEach(module('hudl_mlb'));
    beforeEach(inject(function (_GameService_) {
      GameService = _GameService_;
    }));

    it('should set team logos', function () {
      GameService.setGames([
        {
          game: {
            id: 'gameId1',
            home: { name: 'Home Team Name' },
            away: { name: 'Away Team Name' }
          }
        }
      ]);
      var games = GameService.getGames();
      expect(games[0].home.logo).toEqual('/images/home_team_name.gif');
      expect(games[0].away.logo).toEqual('/images/away_team_name.gif');
    });
  })
}());
