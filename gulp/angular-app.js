var gulp = require('gulp');
var gutil = require('gulp-util');
var plumber = require('gulp-plumber');
var babel = require('gulp-babel')
var concat = require('gulp-concat')
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');

gulp.task('angular:app:develop', function() {
  gulp.src(['./src/public/app/**/*.js'])
  .pipe(plumber())
  .pipe(babel())
  .pipe(concat('app.js'))
  .pipe(gulp.dest('./build/public/js'));
});

gulp.task('angular:app:deploy', function() {
  gulp.src(['./src/public/app/**/*.js'])
  .pipe(plumber())
  .pipe(babel())
  .pipe(concat('app.js'))
  .pipe(uglify())
  .pipe(gulp.dest('./build/public/js'));
});
