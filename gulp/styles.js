var gulp = require('gulp');
var plumber = require('gulp-plumber');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');
var rename = require('gulp-rename');

gulp.task('sass:develop', function() {
  gulp.src('./src/public/sass/*.scss')
  .pipe(plumber())
  .pipe(sass())
  .on('error', function(err) {
    console.log(err);
    this.end();
  })
  .pipe(gulp.dest('./build/public/stylesheets'));
});

gulp.task('sass:deploy', function() {
  gulp.src('./src/public/sass/*.scss')
  .pipe(plumber())
  .pipe(sourcemaps.init())
  .pipe(sass({
    outputStyle: 'compressed'
  }))
  .pipe(sourcemaps.write('./maps'))
  .pipe(rename(function(path) {
    path.basename = path.basename + '.min';
  }))
  // .pipe(gzip({append: false}))
  .on('error', function(err) {
    console.log(err);
    this.end();
  })
  .pipe(gulp.dest('./build/public/stylesheets'));
});
